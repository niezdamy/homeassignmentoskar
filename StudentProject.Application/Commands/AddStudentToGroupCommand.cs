﻿using System;
using MediatR;

namespace StudentProject.Application.Commands
{
    public class AddStudentToGroupCommand : IRequest<bool>
    {
        public Guid GroupId { get; set; }
        public Guid StudentId { get; set; }
    }
}