﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StudentProject.Application.Exceptions;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Application.Commands
{
    public class AddStudentToGroupCommandHandler : IRequestHandler<AddStudentToGroupCommand, bool>
    {
        private readonly StudentProjectDbContext _context;

        public AddStudentToGroupCommandHandler(StudentProjectDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(AddStudentToGroupCommand request, CancellationToken cancellationToken)
        {
            var student = await _context.Students.FindAsync(request.StudentId);
            if (student == null)
            {
                throw new StudentNotFoundException(request.StudentId);
            }

            var group = await _context.Groups.FindAsync(request.GroupId);
            if (group == null)
            {
                throw new GroupNotFoundException(request.GroupId);
            }

            if (IsGroupInOtherProject(request.StudentId, request.GroupId))
            {
                group.Students.Add(student);
                _context.Groups.Update(group);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        private bool IsGroupInOtherProject(Guid studentId, Guid groupId)
        {
            var currentUserProjects = _context.Projects
              .Where(x => x.Groups
              .Any(y => y.Students
              .Any(z => z.StudentId == studentId)))
              .ToList();

            var groupProject = _context.Projects
                .FirstOrDefault(x => x.Groups
                .Any(y => y.GroupId == groupId));

            if (currentUserProjects.Exists(x => x.ProjectId == groupProject.ProjectId))
            {
                return false;
            }

            return true;
        }
    }
}