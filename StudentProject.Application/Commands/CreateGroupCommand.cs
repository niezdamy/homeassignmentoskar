﻿using System;
using MediatR;

namespace StudentProject.Application.Commands
{
    public class CreateGroupCommand : IRequest<Guid>
    {
        public Guid ProjectId { get; set; }
        public string GroupName { get; set; }
    }
}