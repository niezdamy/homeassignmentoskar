﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using StudentProject.Application.Exceptions;
using StudentProject.Domain;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Application.Commands
{
    public class CreateGroupCommandHandler : IRequestHandler<CreateGroupCommand, Guid>
    {
        private readonly StudentProjectDbContext _context;

        public CreateGroupCommandHandler(StudentProjectDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateGroupCommand request, CancellationToken cancellationToken)
        {
            var projectEntity = await _context.Projects.FindAsync(request.ProjectId);

            if (projectEntity == null)
            {
                throw new ProjectNotFoundException(request.ProjectId);
            }

            var newGroup = new GroupEntity
            {
                GroupId = Guid.NewGuid(),
                Name = request.GroupName
            };

            projectEntity.Groups.Add(newGroup);

            _context.Projects.Update(projectEntity);
            await _context.SaveChangesAsync();

            return newGroup.GroupId;
        }
    }
}