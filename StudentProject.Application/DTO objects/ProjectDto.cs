﻿using System;
using System.Collections.Generic;
using AutoMapper;
using StudentProject.Domain;

namespace StudentProject.Application
{
    public class ProjectDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<GroupDto> Groups { get; set; }
    }
}