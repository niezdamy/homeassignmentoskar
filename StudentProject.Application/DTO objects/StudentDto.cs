﻿using System;
using AutoMapper;
using StudentProject.Domain;

namespace StudentProject.Application
{
    public class StudentDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}