﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using MediatR;

namespace StudentProject.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplicationLayer(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}
