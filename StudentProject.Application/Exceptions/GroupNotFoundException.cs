﻿using System;
using System.Runtime.Serialization;

namespace StudentProject.Application.Exceptions
{
    [Serializable]
    internal class GroupNotFoundException : Exception
    {
        private Guid groupId;

        public GroupNotFoundException()
        {
        }

        public GroupNotFoundException(Guid groupId)
        {
            this.groupId = groupId;
        }

        public GroupNotFoundException(string message) : base(message)
        {
        }

        public GroupNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GroupNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}