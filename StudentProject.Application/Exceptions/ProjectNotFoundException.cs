﻿using System;
using System.Runtime.Serialization;

namespace StudentProject.Application.Exceptions
{
    [Serializable]
    internal class ProjectNotFoundException : Exception
    {
        private Guid projectId;

        public ProjectNotFoundException()
        {
        }

        public ProjectNotFoundException(Guid projectId)
        {
            this.projectId = projectId;
        }

        public ProjectNotFoundException(string message) : base(message)
        {
        }

        public ProjectNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProjectNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}