﻿using System;
using System.Runtime.Serialization;

namespace StudentProject.Application.Exceptions
{
    [Serializable]
    internal class StudentNotFoundException : Exception
    {
        private Guid studentId;

        public StudentNotFoundException()
        {
        }

        public StudentNotFoundException(Guid studentId)
        {
            this.studentId = studentId;
        }

        public StudentNotFoundException(string message) : base(message)
        {
        }

        public StudentNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected StudentNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}