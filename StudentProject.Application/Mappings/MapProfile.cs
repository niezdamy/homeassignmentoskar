﻿using System;
using AutoMapper;
using StudentProject.Domain;

namespace StudentProject.Application
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<GroupEntity, GroupDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.GroupId));

            CreateMap<GroupDto, GroupEntity>()
                .ForMember(d => d.GroupId, opt => opt.MapFrom(src => src.Id));

            CreateMap<ProjectEntity, ProjectDto>()
               .ForMember(d => d.Id, opt => opt.MapFrom(src => src.ProjectId));

            CreateMap<ProjectDto, ProjectEntity>()
                .ForMember(d => d.ProjectId, opt => opt.MapFrom(src => src.Id));

            CreateMap<StudentEntity, StudentDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.StudentId));

            CreateMap<StudentDto, StudentEntity>()
                .ForMember(d => d.StudentId, opt => opt.MapFrom(src => src.Id));
        }
    }
}
