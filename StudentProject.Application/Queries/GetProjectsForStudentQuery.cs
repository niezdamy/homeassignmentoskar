﻿using System;
using System.Collections.Generic;
using MediatR;

namespace StudentProject.Application.Queries
{
    public class GetProjectsForStudentQuery : IRequest<IEnumerable<ProjectDto>>
    {
        public Guid StudentId { get; set; }
    }
}