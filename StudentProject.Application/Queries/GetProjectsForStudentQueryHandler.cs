﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentProject.Domain;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Application.Queries
{
    public class GetProjectsForStudentQueryHandler : IRequestHandler<GetProjectsForStudentQuery, IEnumerable<ProjectDto>>
    {
        public StudentProjectDbContext _context;
        private readonly IMapper _mapper;

        public GetProjectsForStudentQueryHandler(StudentProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDto>> Handle(GetProjectsForStudentQuery request, CancellationToken cancellationToken)
        {
            return await _context.Set<ProjectEntity>()
                    .Where(x => x.Groups
                        .Any(y => y.Students
                            .Any(z => z.StudentId == request.StudentId)))
                    .Include(x => x.Groups)
                    .ThenInclude(y => y.Students)
                    .ProjectTo<ProjectDto>(_mapper.ConfigurationProvider)
                    .ToListAsync();
        }
    }
}