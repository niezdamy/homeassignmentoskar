﻿using System;
using System.Collections.Generic;
using MediatR;

namespace StudentProject.Application.Queries
{
    public class GetProjectsListQuery : IRequest<IEnumerable<ProjectDto>>
    {
    }
}