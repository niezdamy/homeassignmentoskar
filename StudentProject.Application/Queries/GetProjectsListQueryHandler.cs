﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Application.Queries
{
    public class GetProjectsListQueryHandler : IRequestHandler<GetProjectsListQuery, IEnumerable<ProjectDto>>
    {
        private readonly StudentProjectDbContext _context;
        private readonly IMapper _mapper;

        public GetProjectsListQueryHandler(StudentProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDto>> Handle(GetProjectsListQuery query, CancellationToken cancellationToken)
        {
            return await _context.Projects.ProjectTo<ProjectDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}
