﻿using System;
using System.Collections.Generic;
using MediatR;

namespace StudentProject.Application.Queries
{
    public class GetStudentsListQuery : IRequest<IEnumerable<StudentDto>>
    {
    }
}