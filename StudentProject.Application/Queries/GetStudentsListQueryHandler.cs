﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Application.Queries
{
    public class GetStudentsListQueryHandler : IRequestHandler<GetStudentsListQuery, IEnumerable<StudentDto>>
    {
        private readonly StudentProjectDbContext _context;
        private readonly IMapper _mapper;

        public GetStudentsListQueryHandler(StudentProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<StudentDto>> Handle(GetStudentsListQuery query, CancellationToken cancellationToken)
        {

            return await _context.Students.ProjectTo<StudentDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}
