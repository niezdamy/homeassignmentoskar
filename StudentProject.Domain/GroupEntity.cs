﻿using System;
using System.Collections.Generic;

namespace StudentProject.Domain
{
    public class GroupEntity
    {
        public GroupEntity()
        {
            Students = new HashSet<StudentEntity>();
        }
        public Guid GroupId { get; set; }
        public string Name { get; set; }
        public ICollection<StudentEntity> Students { get; set; }
    }
}