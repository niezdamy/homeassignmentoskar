﻿using System;
using System.Collections.Generic;

namespace StudentProject.Domain
{
    public class ProjectEntity
    {
        public ProjectEntity()
        {
            Groups = new HashSet<GroupEntity>();
        }

        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public ICollection<GroupEntity> Groups { get; set; }
    }
}