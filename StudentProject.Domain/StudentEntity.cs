﻿using System;

namespace StudentProject.Domain
{
    public class StudentEntity
    {
        public Guid StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}