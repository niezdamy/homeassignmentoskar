using Microsoft.EntityFrameworkCore;
using StudentProject.Domain;

namespace StudentProject.Persistence.DBContext
{
    public class StudentProjectDbContext : DbContext
    {
        public StudentProjectDbContext(DbContextOptions<StudentProjectDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentEntity>().HasKey(x => x.StudentId);
            modelBuilder.Entity<StudentEntity>().ToTable("Student");

            modelBuilder.Entity<GroupEntity>().HasKey(x => x.GroupId);
            modelBuilder.Entity<GroupEntity>().ToTable("Group");

            modelBuilder.Entity<ProjectEntity>().HasKey(x => x.ProjectId);
            modelBuilder.Entity<ProjectEntity>().ToTable("Project");

        }
        public DbSet<StudentEntity> Students { get; set; }

        public DbSet<GroupEntity> Groups { get; set; }

        public DbSet<ProjectEntity> Projects { get; set; }
    }
}