﻿using AutoMapper;
using StudentProject.Application;

namespace StudentProject.Test
{
    public class AutoMapperFactory
    {
        public static IMapper Create()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MapProfile());
            });

            return mappingConfig.CreateMapper();
        }
    }
}
