﻿using System;
using AutoMapper;
using StudentProject.Persistence.DBContext;
using Xunit;

namespace StudentProject.Test
{
    public class QueryTestFixture : IDisposable
    {
        public StudentProjectDbContext Context { get; set; }
        public IMapper Mapper { get; set; }

        public QueryTestFixture()
        {
            Context = TestContextFactory.Create();
            Mapper = AutoMapperFactory.Create();
        }

        public void Dispose()
        {
            TestContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}
