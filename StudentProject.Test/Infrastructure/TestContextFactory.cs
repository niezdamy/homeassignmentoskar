﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using StudentProject.Domain;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Test
{
    public class TestContextFactory
    {
        public static StudentProjectDbContext Create()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.test.json", false, true);

            var configuration = builder.Build();

            var _dbConnection = new SqliteConnection(configuration.GetConnectionString("StudentProjectDataBaseTest"));

            _dbConnection.Open();

            var _dbContextOptions = new DbContextOptionsBuilder<StudentProjectDbContext>()
                .UseSqlite(_dbConnection)
                .Options;

            var context = new StudentProjectDbContext(_dbContextOptions);
            context.Database.EnsureCreated();

            SeedTestData(context);

            return context;
        }

        public static void Destroy(StudentProjectDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }

        public static void SeedTestData(StudentProjectDbContext context)
        {
            var projectList = new List<ProjectEntity>
            {
                new ProjectEntity
                {
                    ProjectId = Guid.NewGuid(),
                    Name = "Test Project 1",
                    Groups = new List<GroupEntity>
                    {
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "Test G1 in P1",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Test Student 1",
                                    LastName = "G1 P1"
                                }
                            }
                        },
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "Test G2 in P1",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Test Student 2",
                                    LastName = "In G2 P1"
                                }
                            }
                        }
                    }
                },
                new ProjectEntity
                {
                    ProjectId = Guid.NewGuid(),
                    Name = "Test Project 2",
                    Groups = new List<GroupEntity>
                    {
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "Test G1 in P2",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Test Student 3",
                                    LastName = "In G1 P2"
                                }
                            }
                        }
                    }
                }
            };

            context.Projects.AddRange(projectList);
            context.SaveChanges();
        }
    }
}
