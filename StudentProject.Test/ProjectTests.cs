using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Shouldly;
using StudentProject.Application;
using StudentProject.Application.Commands;
using StudentProject.Application.Queries;
using StudentProject.Persistence.DBContext;
using StudentProject.Test;
using Xunit;

namespace StudentProject.TestContextFactory
{
    [Collection("QueryCollection")]
    public class ProjectTests
    {
        private readonly StudentProjectDbContext _context;
        private readonly IMapper _mapper;

        public ProjectTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task GetAllProjectsTests()
        {
            var handler = new GetProjectsListQueryHandler(_context, _mapper);
            var result = await handler.Handle(new GetProjectsListQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<ProjectDto>>();
            result.Count().ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task AddNewGroupTest()
        {
            var handler = new CreateGroupCommandHandler(_context);
            var project = _context.Projects.LastOrDefault();
            var newGroupName = "Test NewGroup";

            var groupCountBeforeChanges = project.Groups.Count();

            var result = await handler.Handle(new CreateGroupCommand { ProjectId = project.ProjectId, GroupName = newGroupName}, CancellationToken.None);

            result.ShouldBeOfType<Guid>();

            project.Groups.Count().ShouldBeGreaterThan(groupCountBeforeChanges);
        }
    }
}