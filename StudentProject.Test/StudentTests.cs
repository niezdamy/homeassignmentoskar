using System.Linq;
using System.Threading.Tasks;
using Xunit;
using StudentProject.Persistence.DBContext;
using StudentProject.Test;
using StudentProject.Application.Commands;
using StudentProject.Application.Queries;
using AutoMapper;
using System.Threading;
using StudentProject.Application;
using System.Collections.Generic;
using Shouldly;

namespace StudentProject.TestContextFactory
{
    [Collection("QueryCollection")]
    public class StudentTests
    {
        private readonly StudentProjectDbContext _context;
        private readonly IMapper _mapper;

        public StudentTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task AddStudentToGroupTest()
        {
            var student = _context.Students.FirstOrDefault();
            var otherGroup = _context.Groups.LastOrDefault();

            var handler = new AddStudentToGroupCommandHandler(_context);
            var result = await handler.Handle(new AddStudentToGroupCommand { StudentId = student.StudentId, GroupId = otherGroup.GroupId }, CancellationToken.None);

            result.ShouldBeOfType<bool>();
            result.ShouldBe(true);
        }

        [Fact]
        public async Task AddStudentToSameGroupShouldReturnFalseTest()
        {
            var student = _context.Students.FirstOrDefault();
            var sameGroup = _context.Groups.FirstOrDefault();

            var handler = new AddStudentToGroupCommandHandler(_context);
            var result = await handler.Handle(new AddStudentToGroupCommand { StudentId = student.StudentId, GroupId = sameGroup.GroupId }, CancellationToken.None);

            result.ShouldBeOfType<bool>();
            result.ShouldBe(false);
        }

        [Fact]
        public async Task AddStudentToSameProjectShouldReturnFalseTest()
        {
            var student = _context.Students.FirstOrDefault();
            var otherGroupInTheSameProject = _context.Groups.Skip(1).FirstOrDefault();

            var handler = new AddStudentToGroupCommandHandler(_context);
            var result = await handler.Handle(new AddStudentToGroupCommand { StudentId = student.StudentId, GroupId = otherGroupInTheSameProject.GroupId }, CancellationToken.None);

            result.ShouldBeOfType<bool>();
            result.ShouldBe(false);
        }

        [Fact]
        public async Task GetProjectsForStudentTest()
        {

            var student = _context.Students.FirstOrDefault();
            var handler = new GetProjectsForStudentQueryHandler(_context, _mapper);
            var result = await handler.Handle(new GetProjectsForStudentQuery { StudentId = student.StudentId }, CancellationToken.None);

            result.ShouldBeOfType<List<ProjectDto>>();
            result.Count().ShouldBeGreaterThan(0);
        }
    }
}