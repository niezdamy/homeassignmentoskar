﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentProject.Application;
using StudentProject.Application.Commands;
using StudentProject.Application.Queries;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : BaseController
    {
        private readonly StudentProjectDbContext _context;

        public ProjectController(StudentProjectDbContext context)
        {
            _context = context;
        }

        [HttpPost("Init")]
        public async Task<ActionResult> Init()
        {
            var result = await DataInitializer.SeedProjects(_context);

            return Ok("Sucessfully created entities: " + result);
        }

        [HttpGet("List")]
        public async Task<ActionResult<IEnumerable<StudentDto>>> List()
        {
            return Ok(await Mediator.Send(new GetProjectsListQuery()));
        }

        [HttpPost("CreateGroup")]
        public async Task<ActionResult<Guid>> CreateGroup(Guid projectId, string groupName)
        {
            return Ok(await Mediator.Send(new CreateGroupCommand{ ProjectId = projectId, GroupName = groupName }));
        }

        [HttpPost("AddStudentToGroup")]
        public async Task<ActionResult<bool>> AddStudentToGroup(Guid groupId, Guid studentId)
        {
            return Ok(await Mediator.Send(new AddStudentToGroupCommand{ GroupId = groupId, StudentId = studentId }));
        }
    }
}