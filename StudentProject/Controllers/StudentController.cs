﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentProject.Application;
using StudentProject.Application.Queries;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : BaseController
    {
        private readonly StudentProjectDbContext _context;

        public StudentController(StudentProjectDbContext context)
        {
            _context = context;
        }

        [HttpPost("Init")]
        public async Task<ActionResult> Init()
        {
            var result = await DataInitializer.SeedStudents(_context);
            return Ok("Sucessfully created entities: " + result);
        }

        [HttpGet("List")]
        public async Task<ActionResult<IEnumerable<StudentDto>>>List()
        {
            return Ok(await Mediator.Send(new GetStudentsListQuery()));
        }

        [HttpGet("GetProjects")]
        public async Task<ActionResult<IEnumerable<ProjectDto>>> GetProjects(Guid studentId)
        {
            return Ok(await Mediator.Send(new GetProjectsForStudentQuery{ StudentId = studentId }));
        }
    }
}