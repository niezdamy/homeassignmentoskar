﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StudentProject.Domain;
using StudentProject.Persistence.DBContext;

namespace StudentProject.Web
{
    public static class DataInitializer
    {
        public static void CleanUp(StudentProjectDbContext context)
        {
            context.Students.RemoveRange(context.Students);
            context.SaveChanges();

            context.Groups.RemoveRange(context.Groups);
            context.SaveChanges();

            context.Projects.RemoveRange(context.Projects);
            context.SaveChanges();
        }

        public static async Task<int> SeedStudents(StudentProjectDbContext context)
        {
            var studentsList = new List<StudentEntity>
            {
                new StudentEntity
                {
                    StudentId = Guid.NewGuid(),
                    FirstName = "Andrzej",
                    LastName = "Sapkowski"
                },
                new StudentEntity
                {
                    StudentId = Guid.NewGuid(),
                    FirstName = "Andrzej",
                    LastName = "Pilipiuk"
                },

                new StudentEntity
                {
                    StudentId = Guid.NewGuid(),
                    FirstName = "Stanisław",
                    LastName = "Lem"
                }
            };

            context.Students.AddRange(studentsList);
            return await context.SaveChangesAsync();
        }

        public static async Task<int> SeedProjects(StudentProjectDbContext context)
        {
            var projectList = new List<ProjectEntity>
            {
                new ProjectEntity
                {
                    ProjectId = Guid.NewGuid(),
                    Name = "Project First",
                    Groups = new List<GroupEntity>
                    {
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "G1 in P1",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Student 1",
                                    LastName = "In G1 P1"
                                }
                            }
                        },
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "G2 in P1",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Student 2",
                                    LastName = "In G2 P1"
                                }
                            }
                        },
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "G3 in P1",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Student 3",
                                    LastName = "In G3 P1"
                                }
                            }
                        }
                    }
                },
                new ProjectEntity
                {
                    ProjectId = Guid.NewGuid(),
                    Name = "Project Second",
                    Groups = new List<GroupEntity>
                    {
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "G1 in P2",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Student 4",
                                    LastName = "In G1 P2"
                                }
                            }
                        },
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "G2 in P2",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Student 5",
                                    LastName = "In G2 P2"
                                }
                            }
                        },
                        new GroupEntity
                        {
                            GroupId = Guid.NewGuid(),
                            Name = "G3 in P2",
                            Students = new List<StudentEntity>
                            {
                                new StudentEntity
                                {
                                    StudentId = Guid.NewGuid(),
                                    FirstName = "Student 6",
                                    LastName = "In G3 P2"
                                }
                            }
                        }
                    }
                }
            };

            context.Projects.AddRange(projectList);
            return await context.SaveChangesAsync();
        }
    }
}
