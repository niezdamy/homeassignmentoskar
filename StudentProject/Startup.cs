﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentProject.Application;
using StudentProject.Persistence;
using StudentProject.Persistence.DBContext;
using StudentProject.Web;
using Swashbuckle.AspNetCore.Swagger;

namespace StudentProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(
                options =>
                {
                    options.CheckConsentNeeded = context => true;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                }
            );

            services.AddApplicationLayer();

            services.AddDbContext<StudentProjectDbContext>(
                options =>
                    options.UseSqlite(Configuration.GetConnectionString("StudentProjectDatabase"))
                    
            );
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Home Assignment API",
                    Version = "v1"
                });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}"
                    );
                }
            );

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = app.ApplicationServices.CreateScope()
                    .ServiceProvider.GetService<StudentProjectDbContext>();

                DataInitializer.CleanUp(context);
            }

            app.UseSwagger();
            app.UseSwaggerUI(
                c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Home Assignment API v1");
                }
            );
        }
    }
}
